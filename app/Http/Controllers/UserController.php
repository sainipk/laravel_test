<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Validator;
use Auth;

class UserController extends Controller
{
    

    public function index()
    {
      $users = User::whereHas('roles', function($query){
                $query->where('name', 'employee');
              })->get();

             
      return view('employee.index', compact('users'));
    }

    public function create()
    {
        
        $roles = Role::pluck('name','name')->all();
        return view('employee.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
            'building_no' => 'required',
            'street_name' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'pincode' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors'=>$validator->errors()->getMessages(),'status'=>false]);
             }
             else{
        $user = User::create([
                  'name'      =>  $request->name,
                  'email'     =>  $request->email,
                  'password'  =>  Hash::make($request->password),
                ]);

        $user->assignRole('employee');
        $user->address()->create(
            [
               
                "building_no" => $request->building_no,
                "street_name" => $request->street_name,
                'city' => $request->city,
                'state' => $request->state,
                'country' => $request->country,
                'pincode' => $request->pincode,
               
            ]
        );
        $details['email'] = $request->email;
  
    dispatch(new \App\Jobs\SendEmailJob($details));
        return response()->json(['errors'=>[],'status' => 'true']);
        
            }
       
    }

    public function show($id)
    {
        $user = User::find($id);

        return view('employee.show',compact('user'));
    }

    public function edit($id)
    {
        $user = User::with('address')->where('id',$id)->first();
        return view('employee.edit', compact('user'));
    }

    public function update(Request $request,$id)
    {
       
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'building_no' => 'required',
            'street_name' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'pincode' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors'=>$validator->errors()->getMessages(),'status'=>false]);
             }
             else{
                $user = User::find($id);
                $user->name = $request->name;
                $user->address->building_no = $request->building_no;
                $user->address->street_name = $request->street_name;
                $user->address->city = $request->city;
                $user->address->state = $request->state;
                $user->address->country = $request->country;
                $user->address->pincode = $request->pincode;
                $user->push();
        
        return response()->json(['errors'=>[],'status' => 'true']);
        
    }
    }
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');

    }
}
