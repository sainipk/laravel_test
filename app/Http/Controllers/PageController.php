<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;

class PageController extends Controller
{
    //

    public function index()
    {
        $blogs= Blog::with('blog_cat')->paginate(5);
        return view('index',compact('blogs'));
    }

    public function blog($slug)
    {
        $blog = Blog::where('slug',$slug)->first();
        return view('blog',compact('blog'));
    }

    public function search(Request $request){
        // Get the search value from the request
        $search = $request->input('search');
        $data = $request->query();
        // Search in the title and body columns from the posts table
        $blogs = Blog::query()
            ->where('title', 'LIKE', "%{$search}%")
            ->orWhere('content', 'LIKE', "%{$search}%")
            ->paginate(5);
            return view('search',compact('blogs','data'));
        // Return the search view with the resluts compacted
        //return view('search', compact('posts'));
    }
}
