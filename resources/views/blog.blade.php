@extends('layouts.front')
@section('content')
<!-- page title -->
<section class="page-title bg-primary position-relative">
  <div class="container">
    <div class="row">
      <div class="col-12 text-center">
        <h1 class="text-white font-tertiary">Blogs</h1>
      </div>
    </div>
  </div>
  <!-- background shapes -->
  <img src="{{url('assets/front/images/illustrations/page-title.png')}}" alt="illustrations" class="bg-shape-1 w-100">
  <img src="{{url('assets/front/images/illustrations/leaf-pink-round.png')}}" alt="illustrations" class="bg-shape-2">
  <img src="{{url('assets/front/images/illustrations/dots-cyan.png')}}" alt="illustrations" class="bg-shape-3">
  <img src="{{url('assets/front/images/illustrations/leaf-orange.png')}}" alt="illustrations" class="bg-shape-4">
  <img src="{{url('assets/front/images/illustrations/leaf-yellow.png')}}" alt="illustrations" class="bg-shape-5">
  <img src="{{url('assets/front/images/illustrations/dots-group-cyan.png')}}" alt="illustrations" class="bg-shape-6">
  <img src="{{url('assets/front/images/illustrations/leaf-cyan-lg.png')}}" alt="illustrations" class="bg-shape-7">
</section>
<section class="section">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="font-tertiary mb-3">{{$blog->title}}</h3>
        <p class="font-secondary mb-5">Published on {{date("F j, Y",strtotime($blog->created_at))}} by <span class="text-primary">uixgeek</span
            class="text-primary"> on <span>UX design</span></p>
        <div class="content">
          <img src="{{url('assets/front/images/blog/post-1.jpg')}}" alt="post-thumb" class="img-fluid rounded float-left mr-5 mb-4">
          {!!$blog->content!!}
        </div>
      </div>
    </div>
  </div>
</section>
@endsection