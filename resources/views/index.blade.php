@extends('layouts.front')
@section('content')
<!-- page title -->
<section class="page-title bg-primary position-relative">
  <div class="container">
    <div class="row">
      <div class="col-12 text-center">
        <h1 class="text-white font-tertiary">Blogs</h1>
      </div>
    </div>
  </div>
  <!-- background shapes -->
  <img src="{{url('assets/front/images/illustrations/page-title.png')}}" alt="illustrations" class="bg-shape-1 w-100">
  <img src="{{url('assets/front/images/illustrations/leaf-pink-round.png')}}" alt="illustrations" class="bg-shape-2">
  <img src="{{url('assets/front/images/illustrations/dots-cyan.png')}}" alt="illustrations" class="bg-shape-3">
  <img src="{{url('assets/front/images/illustrations/leaf-orange.png')}}" alt="illustrations" class="bg-shape-4">
  <img src="{{url('assets/front/images/illustrations/leaf-yellow.png')}}" alt="illustrations" class="bg-shape-5">
  <img src="{{url('assets/front/images/illustrations/dots-group-cyan.png')}}" alt="illustrations" class="bg-shape-6">
  <img src="{{url('assets/front/images/illustrations/leaf-cyan-lg.png')}}" alt="illustrations" class="bg-shape-7">

</section>
<!-- /page title -->

<!-- blog -->
<section class="section">
  <div class="container">
  <form action="{{ route('search') }}" class="row" method="GET">
            <div class="col-md-3">
              <input type="text" id="name"  name="search" placeholder="Search" class="form-control px-0 mb-4">
            </div>
            <div class="col-lg-2">
              <button class="btn btn-primary w-100" type="submit">Search</button>
            </div>
          </form>
    <div class="row">
      @foreach($blogs as $blog)
        <div class="col-lg-4 col-sm-6 mb-4">
          <article class="card shadow">
            <img class="rounded card-img-top" src="{{url('assets/front/images/blog/post-5.jpg')}}" alt="post-thumb">
            <div class="card-body">
              <h4 class="card-title"><a class="text-dark" href="{{url('blog',$blog->slug)}}">{{$blog->title}}</a>
              </h4>
              <p class="cars-text">{{$blog->short_description}}</p>
              <h6 class="card-title">Category:{{$blog->blog_cat->title}}</h6>
              <a href="{{url('blog',$blog->slug)}}" class="btn btn-xs btn-primary">Read More</a>
            </div>
          </article>
        </div>
    @endforeach
    </div>
    {{-- Pagination --}}
        <div class="d-flex justify-content-center">
            {!! $blogs->links() !!}
        </div>
  </div>
</section>
<!-- /blog -->
@endsection