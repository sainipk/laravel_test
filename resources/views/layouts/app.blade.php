<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Client</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{url('assets/vendors/feather/feather.css')}}">
  <link rel="stylesheet" href="{{url('assets/vendors/ti-icons/css/themify-icons.css')}}">
  <link rel="stylesheet" href="{{url('assets/vendors/css/vendor.bundle.base.css')}}">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="{{url('assets/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">
  <link rel="stylesheet" href="{{url('assets/vendors/ti-icons/css/themify-icons.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('assets/js/select.dataTables.min.css')}}">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{url('assets/css/vertical-layout-light/style.css')}}">
    <script src="{{url('assets/vendors/js/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <link rel="shortcut icon" href="{{url('assets/images/favicon.png')}}" />
</head>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
@include('header')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
@include('sidebar')
      <!-- partial -->
      <div class="main-panel">
@yield('content')
        <!-- content-wrapper ends -->

        <!-- partial -->
        @include('footer')
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->

  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="{{url('assets/vendors/chart.js/Chart.min.js')}}"></script>


  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="{{url('assets/js/off-canvas.js')}}"></script>
  <script src="{{url('assets/js/hoverable-collapse.js')}}"></script>
  <script src="{{url('assets/js/template.js')}}"></script>
  <script src="{{url('assets/js/settings.js')}}"></script>
  <script src="{{url('assets/js/todolist.js')}}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{url('assets/js/dashboard.js')}}"></script>
  <script src="{{url('assets/js/Chart.roundedBarCharts.js')}}"></script>
  <script src="{{url('assets/vendors/sweetalert/sweetalert.min.js')}}"></script>
  <!-- End custom js for this page-->
</script>
@if (Session::has('success'))
    <script>
        swal({
            title: "Success!",
            text: "{{ Session::get('success') }}",
            icon: 'success'
        });
    </script>
@endif
@if (Session::has('error'))
    <script>
        swal({
            title: "Error!",
            text: "{{ Session::get('error') }}",
            icon: 'error'
        });
    </script>
@endif
<script>
$(document).ready(function(){
    $(".employee").on("submit", function(event){
        event.preventDefault();
 
        var formValues= $(this).serialize();
        var action = $(this).attr("action");
        var type = $(this).attr("method");
        $.ajax({
                    url:action,
                    type:type,
                    data:formValues,
                    dataType: "json",
                    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
   'X-Requested-With': 'XMLHttpRequest'
  },
                    success:function(result)
                    {
                      
                       if(result.status==false)
                       {
            $.each(result.errors, function (key, data) {
             
            $.each(data, function (index, v) 
            {
              $('[name='+key+']').next('.text-danger').html(v);
            })
        })

                       }
                       else{
                        $(".employee")[0].reset();
                        swal({



title: "Success!",



text: "Employee Add Successfully",



icon: "success",



});
                       }

                    }

            });
    });
});
</script>
</body>

</html>
