@extends('layouts.app')
@section('content')
<div class="content-wrapper">
  <div class="row">

    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
        <h2 class="pull-left">Blogs</h2>
        @hasrole('admin')
                <a class="btn btn-success text-right mb-3" href="{{ route('blogs.create') }}"> Add Blog</a>
                @endhasrole
          <div class="table-responsive">
          <table class="table table-bordered">
             <tr>
               <th>No</th>
               <th>Title</th>
               <th>Short Description</th>
               <th>Status</th>
               <th>Created</th>
               <th width="280px">Action</th>
             </tr>
             @foreach ($blogs as $key => $blog)
              <tr>
                <td>{{ ++$key }}</td>
                <td>{{ $blog->title }}</td>
                <td>{{ $blog->short_description }}</td>
                <td>{{ $blog->status==1 ? 'Active':'Deactive' }}</td>
                <td>
                {{date("Y-m-d",strtotime($blog->created_at))}}
                </td>
                <td>
                @hasrole('admin')
                   <a class="btn btn-primary" href="{{ route('blogs.edit',$blog->id) }}">Edit</a>
                   @endhasrole
                   
                </td>
              </tr>
             @endforeach
            </table>
            {{-- Pagination --}}
            <div class="d-flex justify-content-center">
            {!! $blogs->links() !!}
            </div>
          </div>
        </div>
      </div>
    </div>


  </div>
</div>
@endsection
