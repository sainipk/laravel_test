@extends('layouts.app')
@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-md-10 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Edit Blog</h4>
          <p class="card-description">
          Edit Blog
          </p>
       
            <form method="post" action="{{ route('blogs.update',$blog->id) }}" class="forms-sample">  
        @csrf
        @method('PUT')
        <div class="form-group">
              <label for="exampleInputUsername1">Title</label>
              <input type="text" name="title" value="{{ $blog->title }}" class="form-control" id="exampleInputUsername1" placeholder="Name">
              @if ($errors->has('title'))

          <span class="text-danger">
          {{ $errors->first('title') }}
          </span>
          @endif
                      
            </div>
           
            <div class="form-group">
              <label for="exampleInputEmail1">Blog Category</label>
             <select class="form-control" name="category_id">
               <option value="">Select</option>
               @foreach($category as $k=>$cat)
               <option value="{{$k}}" {{ $blog->category_id == $k ? 'selected' : '' }}>{{$cat}}</option>
               @endforeach
             </select>
             @if ($errors->has('category_id'))
             <span class="text-danger">
          {{ $errors->first('category_id') }}
          </span>
          @endif
        
            </div>
            
            <div class="form-group">
              <label for="short_description">Short Description</label>
              <input type="text" name="short_description"  class="form-control"  value="{{ $blog->short_description }}" placeholder="Short Description">
           
              <span class="text-danger">
               
               </span>
            </div>
            <div class="form-group">
              <label for="city">Description</label>
              <textarea name="content" class="form-control"  placeholder="Description">{{ $blog->content }}</textarea>
             
              <span class="text-danger">
               
               </span>
            </div>
            
            <div class="form-group">               
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="checkbox" name="status" class="form-check-input" {{$blog->status==1 ? 'checked':'';}}>
                              Status
                            <i class="input-helper"></i></label>
                          </div>
                        </div>
            <button type="submit" class="btn btn-primary mr-2">Submit</button>

          </form>
        </div>
      </div>
    </div>

  </div>
</div>

@endsection
