@extends('layouts.app')
@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-md-10 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Add Employee</h4>
          <p class="card-description">
            Add Employee
          </p>
       
            <form method="post" action="{{ route('users.store') }}" class="forms-sample employee">
        @csrf
            <div class="form-group">
              <label for="exampleInputUsername1">Name</label>
              <input type="text" name="name" value="{{ old('name') }}" class="form-control" id="exampleInputUsername1" placeholder="Name">
             

                <span class="text-danger">
                
                </span>
            
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email</label>
              <input type="email" name="email" value="{{ old('email') }}" class="form-control"  placeholder="Email">
           

                <span class="text-danger">
               
                </span>
              
            </div>
          
         
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password">
          
              <span class="text-danger">

         
              </span>
             
            </div>
            <div class="form-group">
              <label for="exampleInputConfirmPassword1">Confirm Password</label>
              <input type="password" class="form-control" name="password_confirmation"  id="exampleInputConfirmPassword1" placeholder="Password">
            
              <span class="text-danger">
               
               </span>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Building</label>
              <input type="number" name="building_no" value="{{ old('building_no') }}" class="form-control"  placeholder="Building no">
           
              <span class="text-danger">
               
               </span>
            </div><div class="form-group">
              <label for="street_name">Street Name</label>
              <input type="text" name="street_name" value="{{ old('street_name') }}" class="form-control"  placeholder="Email">
           
              <span class="text-danger">
               
               </span>
            </div>
            <div class="form-group">
              <label for="city">City</label>
              <input type="text" name="city" value="{{ old('city') }}" class="form-control"  placeholder="City">
             
              <span class="text-danger">
               
               </span>
            </div>
            <div class="form-group">
              <label for="city">State</label>
              <input type="text" name="state" value="{{ old('state') }}" class="form-control"  placeholder="State">
             
              <span class="text-danger">
               
               </span>
            </div><div class="form-group">
              <label for="city">Country</label>
              <input type="text" name="country" value="{{ old('country') }}" class="form-control"  placeholder="Country">
             
              <span class="text-danger">
               
               </span>
            </div><div class="form-group">
              <label for="city">Pincode</label>
              <input type="text" name="pincode" value="{{ old('pincode') }}" class="form-control"  placeholder="Pincode">
             
              <span class="text-danger">
               
               </span>
            </div>
            <button type="submit" class="btn btn-primary mr-2">Submit</button>

          </form>
        </div>
      </div>
    </div>

  </div>
</div>

@endsection
