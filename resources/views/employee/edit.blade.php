@extends('layouts.app')
@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-md-10 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Edit Employee</h4>
          <p class="card-description">
            Add Employee
          </p>
       
            <form method="post" action="{{ route('users.update',$user->id) }}" class="forms-sample employee">  
        @csrf
        @method('PUT')
            <div class="form-group">
              <label for="exampleInputUsername1">Name</label>
              <input type="text" name="name" value="{{$user->name}}" class="form-control" id="exampleInputUsername1" placeholder="Name">
             

                <span class="text-danger">
                
                </span>
            
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email</label>
              <input type="email" name="email" value="{{$user->email}}" class="form-control"  placeholder="Email" disabled>
           

                <span class="text-danger">
               
                </span>
              
            </div>
          
         
           
            <div class="form-group">
              <label for="exampleInputEmail1">Building</label>
              <input type="number" name="building_no" value="{{$user->address->building_no}}" class="form-control"  placeholder="Building no">
           
              <span class="text-danger">
               
               </span>
            </div><div class="form-group">
              <label for="street_name">Street Name</label>
              <input type="text" name="street_name" value="{{$user->address->street_name}}" class="form-control"  placeholder="Email">
           
              <span class="text-danger">
               
               </span>
            </div>
            <div class="form-group">
              <label for="city">City</label>
              <input type="text" name="city" value="{{$user->address->city}}" class="form-control"  placeholder="City">
             
              <span class="text-danger">
               
               </span>
            </div>
            <div class="form-group">
              <label for="city">State</label>
              <input type="text" name="state" value="{{$user->address->state}}" class="form-control"  placeholder="State">
             
              <span class="text-danger">
               
               </span>
            </div><div class="form-group">
              <label for="city">Country</label>
              <input type="text" name="country" value="{{$user->address->country}}" class="form-control"  placeholder="Country">
             
              <span class="text-danger">
               
               </span>
            </div><div class="form-group">
              <label for="city">Pincode</label>
              <input type="text" name="pincode" value="{{$user->address->pincode}}" class="form-control"  placeholder="Pincode">
             
              <span class="text-danger">
               
               </span>
            </div>
            <button type="submit" class="btn btn-primary mr-2">Submit</button>

          </form>
        </div>
      </div>
    </div>

  </div>
</div>

@endsection
