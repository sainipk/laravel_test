@extends('layouts.app')
@section('content')
<div class="content-wrapper">
  <div class="row">

    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
        <h2 class="pull-left">Employee Management</h2>
        @hasrole('admin')
                <a class="btn btn-success text-right mb-3" href="{{ route('users.create') }}"> Create New Employee</a>
                @endhasrole
          <div class="table-responsive">
          <table class="table table-bordered">
             <tr>
               <th>No</th>
               <th>Name</th>
               <th>Email</th>
               <th>Roles</th>
               <th width="280px">Action</th>
             </tr>
             @foreach ($users as $key => $user)
              <tr>
                <td>{{ ++$key }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                  @if(!empty($user->getRoleNames()))
                    @foreach($user->getRoleNames() as $role)
                       <label class="badge badge-success">{{ $role }}</label>
                    @endforeach
                  @endif
                </td>
                <td>
                @hasrole('admin')
                   <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>
                   @endhasrole
                    <!-- {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!} -->
                </td>
              </tr>
             @endforeach
            </table>
            {{-- Pagination --}}
            <div class="d-flex justify-content-center">
              
            </div>
          </div>
        </div>
      </div>
    </div>


  </div>
</div>
@endsection
