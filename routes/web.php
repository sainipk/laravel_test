<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\BlogController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PageController::class, 'index']);
Route::get('blog/{slug}', [PageController::class, 'blog']);
Route::get('/search', [PageController::class, 'search'])->name('search');;

Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    Route::resource('users', UserController::class);
    Route::get('home', [HomeController::class, 'index'])->name('home');
});
Route::get('/users/create', [UserController::class, 'create'])->name('users.create')->middleware('role:admin');
Route::get('/users/{user}/edit', [UserController::class, 'edit'])->name('users.edit')->middleware('role:admin');
Route::resource('blogs', BlogController::class)->middleware('role:admin');

