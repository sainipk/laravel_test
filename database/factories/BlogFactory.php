<?php

namespace Database\Factories;

use App\Models\Blog;
use App\Models\BlogCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlogFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Blog::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' =>ucfirst($this->faker->word()),
            'short_description' => $this->faker->sentence(5),
            'content' => $this->faker->paragraph(5),
            'category_id' => rand(1, BlogCategory::count()),
            'status'=>1
        ];
    }
}
